/*
** Project: IoTo
**
** File: ioto-esp32-dht11-mqtt.ino
**
** Description: ESP32 https://en.wikipedia.org/wiki/ESP32 microcontroller
** with DHT11 https://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/ air temperature sensor.
**
** Author: Jouko Loikkanen
**
** Prequisities in Arduino IDE
** Tools/Board:"ESP32DevModule"
** Tools/Port:"/dev/ttyUSB0", depends on environment

** Versions:
** 1.0.0-2021067
** - initial version
** 1.1.0-2021121
** - added NTP time to get a timestamp
** - added PWM, Pulse Width Modulation, for LED's https://randomnerdtutorials.com/esp32-pwm-arduino-ide
** - added Deep Sleep https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/sleep_modes.html
**
** License: GNU GPLv3
*/

#define _VERSION_ "1.1.0-2021121"

#include <WiFi.h>
#include <esp_wifi.h>
#include <WebServer.h>
#include <Arduino_JSON.h>
#include <PubSubClient.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include "driver/adc.h"
#include <esp_bt.h>
#include <time.h>

// Deep sleep
#define DEEP_SLEEP_MINUTES 0.5 // Available powerpack's sleep after 40 seconds without use

// https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/log.html 
const char* TAG = "dht11-mqtt";

// ESP32 time starts from zero at startup so ntp is needed to get a timestamp
const char* ntpServer = "pool.ntp.org";
const int gmtOffset_sec = 3600 * 2;
const int daylightOffset_sec = 3600;

// GPIO 2 failed with "Failed to connect to ESP32: Timed out waiting for packet header"
#define DHTPIN 4     // Digital pin connected to the DHT sensor 

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

// See guide for details on sensor wiring and usage:
//   https://learn.adafruit.com/dht/overview

DHT_Unified dht(DHTPIN, DHTTYPE);

// How often update the weather 
const long lWeatherUpdateInterval = 5000;

// LEDs
const int freq = 5000;
const int ledGreenChannel = 0;
const int ledRedChannel = 1;
const int resolution = 8;
const int ledGreen = 19;
const int ledRed = 23;
const int ledOn = 64;
const int ledOff = 0;

// WiFi
const char *ssid = "IoTo";
const char *sspw = "!MoroSanoPoro...";
WiFiClient espClient;

// MQTT
const char *mqttServers[] = {"192.168.1.12", "192.168.1.11", ""};
const int mqttPort = 1883;
const char *mqttTopicPath = "IoT/esp32";
const char *mqttUser = "iuser";
const char *mqttPass = "...KunMunatLeikattiin!";
static char esp32mac[14] = {0};
static char mqttClientId[20] = {0};
static char mqttTopic[32] = {0};

PubSubClient mqttClient(espClient);

// JSON
JSONVar jsonWeather;

// Function declarations
void setupESP32();
void setupMQTT();
void setupWeather();
void setupLED();
boolean connectWiFi();
boolean connectMQTT();
void setLED(int color, int onoff);
void goDeepSleep();

///////////////////////
// Helper fuctions
///////////////////////

// Setup LED's
void setupLED() {

  // configure LED PWM functionalitites
  ledcSetup(ledGreenChannel, freq, resolution);
  ledcSetup(ledRedChannel, freq, resolution);
  
  // attach the channel to the GPIO to be controlled
  ledcAttachPin(ledGreen, ledGreenChannel);
  ledcAttachPin(ledRed, ledRedChannel);

  // Set LED's on
  setLED(ledRedChannel, ledOn);
  setLED(ledGreenChannel, ledOn);
  delay(500);
  setLED(ledRedChannel, ledOff);

  return;
}

// Setup ESP32
void setupESP32() {
  
  // Init jsonWeather
  jsonWeather["name"] = "esp32";

  // Init ESP32 serial interface
  Serial.begin(115200);

  return;
}

// Setup MQTT
void setupMQTT() {

  // Init MQTT
  mqttClient.setSocketTimeout(5); // default 15 is too long - 5 seconds should be enough

  return;
}

// Setup Weather sensor
void setupWeather() {
  
  dht.begin();
  Serial.printf("IoT-II-esp32-dht11-mqtt %s\n", _VERSION_);
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println(F("-------------------------------------"));
  Serial.println(F("Temperature Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("°C"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("°C"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("°C"));
  Serial.printf (  "Min delay:   %d\n", sensor.min_delay);
  Serial.println(F("-------------------------------------"));
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println(F("Humidity Sensor"));
  Serial.print  (F("Sensor Type: ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:  ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:   ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:   ")); Serial.print(sensor.max_value); Serial.println(F("%"));
  Serial.print  (F("Min Value:   ")); Serial.print(sensor.min_value); Serial.println(F("%"));
  Serial.print  (F("Resolution:  ")); Serial.print(sensor.resolution); Serial.println(F("%"));
  Serial.printf (  "Min delay:   %d\n", sensor.min_delay);
  Serial.println(F("-------------------------------------"));

  return;
}

// Go to the deep sleep state to save battery
void goDeepSleep()
{
  Serial.println("Going to sleep...");
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  mqttClient.disconnect();
  btStop();

  adc_power_off();
  esp_wifi_stop();
  esp_bt_controller_disable();

  // Configure the timer to wake us up!
  esp_sleep_enable_timer_wakeup(DEEP_SLEEP_MINUTES * 60L * 1000000L);

  // Sleepy sleepers
  esp_deep_sleep_start();
}

// Returns true if connected to WiFi AP and false otherwise
boolean connectWiFi() {
  static int iRet = 1;
  static time_t now = 0;
  uint8_t mac[6];
  
  if (WiFi.status() == WL_CONNECTED) {
    return true;
  }
  
  Serial.printf("Connecting to WiFi %s #%3d\n", ssid, iRet++);
  WiFi.begin(ssid, sspw);
  while (WiFi.status() != WL_CONNECTED) {
    if (iRet > 10) {
      return false;
    }
    delay(500);
    Serial.printf("Connecting to WiFi %s #%3d\n", ssid, iRet++);
  }
  Serial.printf("connected to %s, got IP ", ssid); Serial.println(WiFi.localIP());

  // MAC is needed for identifying ESP32 client
  WiFi.macAddress(mac);
  sprintf(esp32mac, "%x%x%x%x%x%x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  jsonWeather["mac"] = esp32mac;
  sprintf(mqttClientId, "esp32-%s", esp32mac);
  sprintf(mqttTopic, "%s/%s", mqttTopicPath, esp32mac);

  return true;
}

// Returns true if connected to MQTT server and false otherwise
boolean connectMQTT() {
  static int iRet = 1;
  static char cServer = 0;

  if (!connectWiFi()) {
    return false;
  }
  
  if (mqttClient.connected()) {
    return true;
  }

  while (!mqttClient.connected()) {
    for (cServer = 0; strlen(mqttServers[cServer]); cServer++) {
      Serial.printf("Connecting to MQTT server at %s:%d - ", mqttServers[cServer], mqttPort);
      Serial.printf("mqttClient.connect(%s, %s, %s) - ", mqttClientId, mqttUser, mqttPass);
      mqttClient.setServer(mqttServers[cServer], mqttPort);
      if (mqttClient.connect(mqttClientId, mqttUser, mqttPass)) {
        Serial.println("connected");
        break;
      } else {
        Serial.printf("failed, rc=%d, trying next server\n", mqttClient.state());
        if (iRet++ > 3) {
          Serial.printf("No servers responded, rc=%d, reconnecting after a deep sleep\n", mqttClient.state());
          return false;
        }
        delay(500);
      }
    }
  }
  return true;
}

// Set a LED on/off
void setLED(int color, int onoff) {
  Serial.printf("setLED(%d, %d)\n", color, onoff);
  ledcWrite(color, onoff);
}

// Updates weather details and publish it to MQTT topic
void updateWeather() {
  // static magic - stack no heap fragmentation issues
  static sensors_event_t event;
  static char szTopic[32];
  static char szWeather[128];
  static int iWeather = 0;

  if (connectMQTT() == false) {
    goDeepSleep();  // WiFi or MQTT not available; take a snap and retry
  }

  // Get temperature event
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature event!"));
  } else {
    jsonWeather["temperature"] = event.temperature;
  }
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading humidity event!"));
  } else {
    jsonWeather["humidity"] = event.relative_humidity;
  }

  jsonWeather["date"] = getTimestamp();

  strcpy(szWeather, JSON.stringify(jsonWeather).c_str());
  mqttClient.publish(mqttTopic, szWeather);
  Serial.printf("publish(%s, %s)\n", mqttTopic, szWeather);

  goDeepSleep();

  return;
}

// Get current local time as ISO date
char *getTimestamp()
{
  //static struct tm *timeinfo;
  static time_t timestamp;
  static char szISODate[sizeof "2000-10-10T10:10:10Z"];

  // Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  // Initial time starts from zero of the epoch; few delay is needed
  while (timestamp < 10000) {
    time(&timestamp);
    Serial.printf("time_t now = %ld\n", timestamp);
    delay(100);
  }
  strftime(szISODate, sizeof szISODate, "%FT%TZ", localtime(&timestamp));
  Serial.printf("DEBUG: time = %ld, szISODate = %s\n", timestamp, szISODate);

  return(szISODate);
}

/*
** Arduino program has two "main" functions – setup() and loop()
** 1. setup() is for initializing things
** 2. loop()  is for looping things until terminated
*/

// 1. setup and initialize things
void setup() {

  // LED
  setupLED();

  // ESP32
  setupESP32();

  // MQTT
  setupMQTT();
  
  // Weather
  setupWeather();

}

// 2. loop things until terminated
void loop() {

  static unsigned long currentMillis = 0, previousMillis = 0;

  // Need to update values and publish MQTT?
  currentMillis = millis();
  if (currentMillis - previousMillis >= lWeatherUpdateInterval) {
    updateWeather();
    previousMillis = currentMillis;
  }

}
