# IoTo IoT Measuring Device

Project to create the IoT Measuring Device for the IoTo. This project contains source code for the device. Create, build and deploy it on your environment. Please follow [the documentation](https://www.hackster.io/projects/5ea27e) for the background details to get an idea of the big picture.


## 1. Project Prerequisites

### 1.1. The hardware

Please follow the documentation above on how to build the device.

### 1.2. The software

#### 1.2.1. Clone the repository
Start cloning (or downloading) the repository

* I have used _joker_ as OS level user but default pi or whatever should fine
    * Using a different user is all about separating things. Just in case to limit damage when it hit the fan – kind of insurance
* At `$HOME` I have created a directory `src/ioto` and cloned the repo there. So the project is located at `$HOME/src/ioto-ruuvi-to-mqtt (/home/joker/src/ioto-ruuvi-to-mqtt)` directory. Please feel free to create your own model
* Just a reminder for me how I did it – just skip if it is too obvious for you:
    ```
    cd
    mkdir -p src/ioto
    cd src/ioto
    git clone https://gitlab.com/iot-observatio/ioto-esp32-dht11-mqtt.git
    cd ioto-esp32-dht11-mqtt
    ```


## 2. The firmware

Please see more details on how to use [ESP32 Arduino](https://docs.espressif.com/projects/arduino-esp32/en/latest/) to build up the firmware.


## All things done?
Once you have built the hardware and installed the software in it, deployed as firmware, you can start using it.

That's all for the project setup. By now the data from your `IoT Measuring Device` should be available in `My Cloud Platform`. Enjoy!



